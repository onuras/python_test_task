from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from .models import Vacancy
from .serializers import VacancySerializer


@csrf_exempt
def vacancy_list(request):
    vacancies = Vacancy.objects.all()
    serializer = VacancySerializer(vacancies, many=True)
    return JsonResponse(serializer.data, safe=False)
