from django.contrib import admin

from .models import Vacancy, Company, City


admin.site.register(Vacancy)
admin.site.register(Company)
admin.site.register(City)
