from django.db import models


class City(models.Model):
    location = models.CharField(max_length=200)

    def __str__(self):
        return self.location

    class Meta:
        verbose_name_plural = "cities"


class Company(models.Model):
    name = models.CharField(max_length=200)
    image_list = models.CharField(max_length=1000, null=True)
    logo = models.CharField(max_length=200, null=True)
    location = models.ForeignKey(City)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "companies"


class Vacancy(models.Model):
    title = models.CharField(max_length=200)
    locations = models.ManyToManyField(City)
    company = models.ForeignKey(Company)
    starts_at = models.CharField(max_length=200)
    ends_at = models.CharField(max_length=200)
    description = models.TextField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "vacancies"
