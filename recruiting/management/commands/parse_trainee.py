from django.core.management.base import BaseCommand
from requests import get
from bs4 import BeautifulSoup
from ...models import City, Company, Vacancy


class Command(BaseCommand):
    help = "Parses vacancies from trainee.de"

    def handle(self, *args, **options):
        page = get("https://www.trainee.de/traineestellen/").text
        soup = BeautifulSoup(page, "html.parser")
        jobs = soup.find(id="jobs")
        for (i, job_link) in enumerate(jobs.find_all("a")):
            self._parse_job(job_link.get("href"))

            # Job description says parse first 20 jobs
            if i >= 20:
                break

    def _parse_job(self, link):
        page = get("https://www.trainee.de" + link).text
        soup = BeautifulSoup(page, "html.parser")
        title = soup.find("h2").text

        cities = None
        starts_at = None
        ends_at = None
        for li in soup.find_all("li"):
            text = li.text.strip()
            if text.startswith("Ort:"):
                cities = list(map(str.strip, text[5:].split(",")))
            if text.startswith("Beginn:"):
                starts_at = text[8:]
            if text.startswith("Dauer:"):
                ends_at = text[7:]

        description = soup.find("div", class_="tr-lg-w-2/3").text.strip()

        company_link = None
        for link in soup.find_all("a"):
            if link.text.find("Über das Unternehmen") >= 0:
                company_link = link.get("href")

        company = self._parse_company(company_link)

        # save cities first
        cities_objs = []
        for city in cities:
            (city, created) = City.objects.get_or_create(location=city)
            cities_objs.append(city)

        (vacancy, created) = Vacancy.objects.get_or_create(
            title=title,
            company=company,
            starts_at=starts_at,
            ends_at=ends_at,
            description=description,
        )

        # Add locations
        for city in cities_objs:
            vacancy.locations.add(city)

        print("Vacancy: {} successfully added into database".format(title))

    def _parse_company(self, link):
        page = get("https://www.trainee.de" + link).text
        soup = BeautifulSoup(page, "html.parser")
        logo = soup.find("img", class_="tr-stage__logo").get("src")

        name = None
        location = None
        for li in soup.find_all("li"):
            text = li.text.strip()
            if text.startswith("Name:"):
                name = text[6:].strip()
            if text.startswith("Ort:"):
                location = text[5:].strip()

        image_list = []
        slider = soup.find(attrs={"data-module": "MediaSlider"})
        if slider:
            for img in slider.find_all("img"):
                image_list.append(img.get("src"))

        (city, created) = City.objects.get_or_create(location=location)
        (company, created) = Company.objects.get_or_create(
            name=name,
            logo=logo,
            image_list=",,,".join(image_list),
            location=city,
        )
        return company
