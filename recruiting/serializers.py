from rest_framework import serializers
from .models import Company, Vacancy


class CompanySerializer(serializers.ModelSerializer):
    location = serializers.StringRelatedField(many=False)

    def to_representation(self, obj):
        return {
            "name": obj.name,
            "image_list": obj.image_list.split(",,,"),
            "logo": obj.logo,
            "location": obj.location.location,
        }

    class Meta:
        model = Company
        fields = ("name", "image_list", "logo", "location")


class VacancySerializer(serializers.ModelSerializer):
    locations = serializers.StringRelatedField(many=True)
    company = CompanySerializer(many=False)

    class Meta:
        model = Vacancy
        fields = ("title", "locations", "company",
                  "starts_at", "ends_at", "description", "is_active")
